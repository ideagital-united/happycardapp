import { Geolocation } from '@ionic-native/geolocation';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { OrdersPage } from './../pages/orders/orders';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from './../pages/login/login';
// import { ProfilePage } from './../pages/profile/profile';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from '../providers/rest/rest';

// import { File } from '@ionic-native/file';
// import { Transfer } from '@ionic-native/transfer';
// import { FilePath } from '@ionic-native/file-path';
// import { Camera } from '@ionic-native/camera';

import { FileTransfer } from '@ionic-native/file-transfer';
// import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
// import { Badge } from '@ionic-native/badge';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AppVersion } from '@ionic-native/app-version';

@NgModule({
  declarations: [
    MyApp,
    OrdersPage,
    TabsPage,
    LoginPage,
    // ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp,{
      tabsPlacement: 'top',
      backButtonText: 'กลับ' 
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OrdersPage,
    TabsPage,
    LoginPage,
    // ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NativeStorage,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestProvider,
    FileTransfer,
    // FileUploadOptions,
    // FileTransferObject,
    File,
    Camera,
    // Badge,
    BarcodeScanner,
    AppVersion
  ]
})
export class AppModule {}
