import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  // host = "http://track.ideagital.com/";
  host = "https://htm.ideagital.com/";
  api = "api/messenger/";
  
  constructor(public http: HttpClient) { }

  get(url, token){
    let headers = {
      headers: new HttpHeaders({ 
        'Authorization': 'Bearer ' + token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      })
    };

    // console.log('rest : ' + this.host + this.api + url);
    
    return this.http.get(this.host + this.api + url, headers);
  }

  post(url, data, token){
    let headers = {
      headers: new HttpHeaders({ 
        'Authorization': 'Bearer ' + token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      })
    };

    return this.http.post(this.host + this.api + url, JSON.stringify(data), headers);
  }


  getToken(input){
    let headers = {
      headers: new HttpHeaders({ 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      })
    };

    let data = {
      "grant_type" : "password",
      "client_id" : "2",
      "client_secret" : "fRfvscaKt4tqikgMMNF7aVwPxMhBukYdNyZk6aYY",
      "username" : input.username + '@happycard.io',
      "password" : input.password
    };

    return this.http.post(this.host + 'oauth/token', JSON.stringify(data), headers);
  }

  getDistance(){
    return this.http.get(this.host+'api/distance');
  }

}
