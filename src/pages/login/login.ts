import { NativeStorage } from '@ionic-native/native-storage';
import { Geolocation } from '@ionic-native/geolocation';
import { RestProvider } from './../../providers/rest/rest';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = { username: '', password: '' };
  auth: any;
  latitude = 0;
  longitude = 0;

  // Get #map element
  @ViewChild('map') mapElement: ElementRef;

  // Google Map object
  map: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public rest: RestProvider, 
    private nativeStorage: NativeStorage,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public geolocation: Geolocation
  ) 
  {

  }

  submitLogin() {
    const loader = this.loadingCtrl.create({
      // content: "กำลังโหลด...",
      dismissOnPageChange: true,
    });
    loader.present();

    this.rest.getToken(this.user)
      .subscribe(
        (data:any) => {
          // this.auth = data.access_token;
          this.storeAuth(data.access_token);
          // console.log(this.auth);
        },
        (error) => { 
          // alert(JSON.stringify(error, null, 4));
          console.error(error.message);
          loader.dismiss();
          this.presentAlert('เข้าสู่ระบบไม่สำเร็จ', 'เบอร์โทรหรือรหัสผ่านไม่ถูกต้อง');
        }
      );
  }

  storeAuth(token) {
    this.nativeStorage.setItem('auth', {accessToken: token})
    .then(
      () => {
        console.log('Stored item!');
        this.appReload();
      },
      error => console.error('Error storing item', error)
    );
  }

  /** Reload app for check login again */
  appReload() {
    window.location.reload();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "กำลังโหลด...",
      dismissOnPageChange: true,
      // duration: 3000
    });
    loader.present();
  }

  presentAlert(title='', message='') {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  getLocation() {

    let loader = this.loadingCtrl.create();
    loader.present();


    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((resp) => {

      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      let accuracy = resp.coords.accuracy;

      alert(this.latitude + ' ' + this.longitude + ', accuracy: ' + accuracy);

      let latLng = new google.maps.LatLng(this.latitude, this.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      // let marker = new google.maps.Marker({
      //   map: this.map,
      //   animation: google.maps.Animation.DROP,
      //   position: latLng
      // });

      // https://developers.google.com/maps/documentation/javascript/examples/layer-traffic
      let trafficLayer = new google.maps.TrafficLayer();
      trafficLayer.setMap(this.map);

      loader.dismiss();
    });
  }

  // getDistance(){

  //   this.rest.getDistance()
  //     .subscribe(
  //       (data:any) => {
  //         console.log(data);
  //       },
  //       (error) => { 
  //         console.log(error.message);
  //       }
  //     );
  // }

}
