import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from './../../providers/rest/rest';
import { Geolocation } from '@ionic-native/geolocation';
// import { Badge } from '@ionic-native/badge';

/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  status = 'pending';
  orders = [];
  order:object;
  location:string = '';
  loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private nativeStorage: NativeStorage,
    public platform: Platform,
    public rest: RestProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public geolocation: Geolocation,
    // private badge: Badge
  ) {

    // Load when opened
    this.platform.ready().then(
      ()=>{
        this.getLocation();
        // this.getOrders('pending');
      },
      ()=>console.log('Platform not ready!')
    );
  }

  ionViewWillEnter() {
    this.getLocation();
    this.getOrders(this.status);
  }


  getOrders(status) {
    this.getLocation();
    this.nativeStorage.getItem('auth').then(
      auth => {

        this.loading = this.loadingCtrl.create({
          dismissOnPageChange: true,
        });
        this.loading.present();
        
        /** Load orders data */
        this.rest.get('orders/' + status + '?location=' + this.location, auth.accessToken)
          .subscribe(
            (res:any) => {
              this.orders = res.data;
              // this.badge.set(res.data.length);
              // this.badge.increase(1);
              // this.badge.clear();
              this.loading.dismiss();
            },
            (error) => { 
              console.log(error.message);
              this.loading.dismiss();
            }
          );

      },
      error => console.error(error)
    );
  }


  getLocation() {
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((resp) => {
      this.location = resp.coords.latitude + ',' + resp.coords.longitude;
    });
  }

  orderDetail(order){
    this.navCtrl.push('OrderPage', order);
  }


  openProfilePage() { 
    this.navCtrl.push('ProfilePage');
  }

  openDeliveryPage(order){
    this.navCtrl.push('DeliveryPage', order);
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad OrdersPage');
  // }

  dismissLoading(){
    if(this.loading){
        this.loading.dismiss();
        this.loading = null;
    }
  }

}
