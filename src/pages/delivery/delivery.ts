import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, ToastController, LoadingController, Content, normalizeURL } from 'ionic-angular';

// import { FileTransfer } from '@ionic-native/file-transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from './../../providers/rest/rest';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


/**
 * Generated class for the DeliveryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.html',
})
export class DeliveryPage {

  @ViewChild(Content) content: Content;

  order:any;
  lastImage: string = null;
  loading:any;

  imageURI:any = null;
  imageFileName:any;

  location:string = '';
  status = "pending";
  orderData:any;
  orderStatus = "";
  barcodeText = "";
  format = "Press button to scan...";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public platform: Platform, 
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController, 
    public toastCtrl: ToastController, 
    public camera: Camera, 
    public transfer: FileTransfer,
    public nativeStorage: NativeStorage,
    public rest: RestProvider,
    public geolocation: Geolocation,
    public barcodeScanner: BarcodeScanner,
  ) {
    this.order = this.navParams.data;
    this.orderStatus = this.navParams.data.delivery.status;

    // Load when opened
    this.platform.ready().then(
      ()=>{
        // Get Location
        this.getLocation();
      },
      ()=>console.log('Platform not ready!')
    );
  }

  orderItems(order){
    this.navCtrl.push('OrderPage', order);
  }

  startDelivery(orderId){
    this.nativeStorage.getItem('auth').then(
      auth => {
        // /** Put order start delivery */
        const loader = this.loadingCtrl.create({
          dismissOnPageChange: true,
        });
        loader.present();

        
        // /** Load orders data */
        this.rest.post('order/' + orderId + '/delivery/start?location=' + this.location, [],auth.accessToken)
          .subscribe(
            (res:any) => {
              loader.dismiss();
              this.order = res.data;
              this.orderData = res.data;
              this.orderStatus = res.data.delivery.status;
              this.content.scrollToBottom(100);
            },
            (error) => { 
              loader.dismiss();
              alert(JSON.stringify(error, null, 4));
              console.log(error.message);
            }
          );
      },
      error => alert(JSON.stringify(error, null, 4))
    );
  }

  finishDelivery(orderId){
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: 'image',
      chunkedMode: false,
      headers: {}
    }
  
    fileTransfer.upload(this.imageURI, 'https://htm.ideagital.com/api/image/upload', options)
      .then((data) => {
        this.imageFileName = JSON.parse(data.response);

        /** Update order status */
        this.nativeStorage.getItem('auth').then(
          auth => {
            // /** Put order start delivery */
            // const loader = this.loadingCtrl.create({
            //   dismissOnPageChange: true,
            // });
            // loader.present();
    
            let data = {
              image: this.imageFileName.url,
              tracking_number: this.barcodeText
            } 
            
            // /** Load orders data */
            this.rest.post('order/' + orderId + '/delivery/finish?location=' + this.location, data,auth.accessToken)
              .subscribe(
                (res:any) => {
                  loader.dismiss();
                  this.order = res.data;
                  this.orderStatus = res.data.delivery.status;
                  this.content.scrollToBottom(100);
                },
                (error) => { 
                  loader.dismiss();
                  console.log(error.message);
                }
              );
          },
          error => alert(JSON.stringify(error, null, 4))
        );
      }, (err) => {
        // console.log(err);
        loader.dismiss();
        this.presentToast(err);
      });
  }

  startScan() {
    this.barcodeScanner.scan().then(
      data => {
        this.barcodeText = data.text;
        this.format = data.format;
      },
      (error) => {
        alert(JSON.stringify(error, null, 4));
      }
    )
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  getLocation() {
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((resp) => {
      this.location = resp.coords.latitude + ',' + resp.coords.longitude;
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true,
    }
  
    this.camera.getPicture(options).then((imagePath) => {
      if(this.platform.is('ios'))
        this.imageURI = normalizeURL(imagePath);
      else
        this.imageURI = imagePath;

      console.log(this.imageURI);
      this.content.scrollToBottom(100);
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }


  // openCamera(pictureSourceType: any) {
  //   let options: CameraOptions = {
  //     quality: 95,
  //     destinationType: this.camera.DestinationType.FILE_URI,
  //     sourceType: pictureSourceType,
  //     encodingType: this.camera.EncodingType.PNG,
  //     targetWidth: 400,
  //     targetHeight: 400,
  //     saveToPhotoAlbum: true,
  //     correctOrientation: true
  //   };
  //   this.camera.getPicture(options).then(imageData => {
  //     if (this.platform.is('ios'))
  //       this.base64Image = normalizeURL(imageData);
  //       // IF problem only occur in ios and normalizeURL 
  //       //not work for you then you can also use 
  //       //this.base64Image= imageData.replace(/^file:\/\//, '');
  //     else
  //       this.base64Image= "data:image/jpeg;base64," + imageData;
  //   }, error => {
  //       console.log('ERROR -> ' + JSON.stringify(error));
  //   });
  // }


  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "กำลังอัพโหลด..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
  
    let options: FileUploadOptions = {
      fileKey: 'image',
      chunkedMode: false,
      // fileName: 'ionicfile',
      // mimeType: "image/jpeg",
      headers: {}
    }
  
    fileTransfer.upload(this.imageURI, 'https://htm.ideagital.com/api/image/upload', options)
      .then((data) => {
        this.imageFileName = JSON.parse(data.response);
        loader.dismiss();
        this.presentToast("อัพโหลดรูปแล้ว");
      }, (err) => {
        console.log(err);
        loader.dismiss();
        this.presentToast(err);
      });
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryPage');
    setTimeout(() => {
        this.content.scrollToBottom(300);
    }, 1000);
  }
}
