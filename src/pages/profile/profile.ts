import { RestProvider } from './../../providers/rest/rest';
import { NativeStorage } from '@ionic-native/native-storage';
import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicPage, NavController, NavParams, Platform, AlertController} from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { AppVersion } from '@ionic-native/app-version';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  profile = [];
  version:any = '';
  location:string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public nativeStorage: NativeStorage, 
    public rest: RestProvider, 
    public platform: Platform,
    public alertCtrl: AlertController,
    public splashScreen: SplashScreen,
    private appVersion: AppVersion,
    public geolocation: Geolocation,
  ) {
    // Load when opened
    this.platform.ready().then(
      ()=>{
        this.nativeStorage.getItem('auth').then(
          auth => this.getProfile(auth.accessToken),
          error => console.error(error)
        );
        
        this.appVersion.getVersionNumber().then((version) => {
          this.version = version;
        });

        this.getLocation();
      },
      (error)=>{ console.log('Platform not ready!') }
    );

  }

  getLocation() {
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((resp) => {
      this.location = resp.coords.latitude + ',' + resp.coords.longitude;
    });
  }

  getProfile(token){
    this.rest.get('profile', token)
      .subscribe(
        (res:any) => {
          this.profile = res.data;
        },
        (error) => { console.log(error.message) }
      );
  }

  logout(){
    this.nativeStorage.remove('auth')
    .then(
      () => {
        this.navCtrl.push(LoginPage);
      },
      (error) => {
        console.log(error)
      }
    );
  }

  alertLocation() {
    this.getLocation();

    const confirm = this.alertCtrl.create({
      title: 'พิกัดตำแหน่ง',
      message: this.location,
      buttons: [
        {
          text: 'ตกลง',
          role: 'cancel',
          handler: () => {
            // console.log('Disagree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  alertLogout() {
    const confirm = this.alertCtrl.create({
      title: '',
      message: 'คุณต้องการออกจากระบบใช่หรือไม่?',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          handler: () => {
            // console.log('Disagree clicked');
          }
        },
        {
          text: 'ออกจากระบบ',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    confirm.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
