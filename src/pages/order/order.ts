import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
// import { NativeStorage } from '@ionic-native/native-storage';
import { RestProvider } from './../../providers/rest/rest';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the OrderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
  
  order:object;
  location:string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    // private nativeStorage: NativeStorage,
    public rest: RestProvider,
    public loadingCtrl: LoadingController,
    public geolocation: Geolocation
  ) {
    this.order = this.navParams.data;
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((resp) => {
      this.location = resp.coords.latitude + ',' + resp.coords.longitude;
    });
  }



  // startDelivery(orderId){
  //   this.nativeStorage.getItem('auth').then(
  //     auth => {
  //       // /** Put order start delivery */
  //       const loader = this.loadingCtrl.create({
  //         dismissOnPageChange: true,
  //       });
  //       loader.present();

        
  //       // /** Load orders data */
  //       this.rest.post('order/' + orderId + '/delivery/start?location=' + this.location, [],auth.accessToken)
  //         .subscribe(
  //           (res:any) => {
  //             loader.dismiss();
  //             // alert(JSON.stringify(res, null, 4));
  //             this.navCtrl.push('DeliveryPage', res.data).then(() => {
  //               let index = 1;
  //               this.navCtrl.remove(index);
  //             });
  //           },
  //           (error) => { 
  //             loader.dismiss();
  //             alert(JSON.stringify(error, null, 4));
  //             console.log(error.message);
  //           }
  //         );
  //     },
  //     error => alert(JSON.stringify(error, null, 4))
  //   );
  // }

  startDelivery(order){
    this.navCtrl.push('DeliveryPage', order).then(() => {
      let index = 1;
      this.navCtrl.remove(index);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
  }

}
